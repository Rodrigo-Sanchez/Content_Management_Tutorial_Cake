<!-- File: src/Template/Articles/edit.ctp -->

<h1>Edit Article</h1>
<?php
    echo $this->Form->create($article);
    echo $this->Form->control('user_id', ['type' => 'hidden']);
    echo $this->Form->control('title');
    echo $this->Form->control('body', ['rows' => '3']);
    echo $this->Form->button(__('Save Article'));
    echo $this->Form->end();

    /* Crea un elemento de selección múltiple que usa la variable $tags 
    para generar las opciones de la caja de selección. */
    echo $this->Form->control('tags._ids', ['options' => $tags]);
?>