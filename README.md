# Content Management Tutorial CakePHP

Tutorial seguido para la creación de una aplicación CMS sencilla.

## Descripción

Este repositorio fue creado para la primera asignación del programa de becarios correspondiente al semestre 2019-II con sede en la *Dirección General de Cómputo y de Tecnologías de Información y Comunicación* de la *Universidad Nacional Autónoma de México*.

### Ligas

Las especificaciones de este tutorial se encuentran [aquí](https://book.cakephp.org/3.0/en/tutorials-and-examples/cms/installation.html).

## Autor

* Sánchez Morales Rodrigo Alejandro.

### Contacto

<rodrigosanchez@ciencias.unam.mx>

## Instructor

* Barajas Gonzalez Daniel.

### Contacto

<danielbg.dev@gmail.com>