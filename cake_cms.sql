﻿-- USE cake_cms;

-- El equivalente de AUTO_INCREMENT de MySQL es SERIAL en PostgreSQL.
-- Se cambió el tipo de dato DATETIME por TIMESTAMP, investigar diferencias.
CREATE TABLE users (
    id       SERIAL PRIMARY KEY,
    email    VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created  TIMESTAMP,
    modified TIMESTAMP
);

-- Modificando la creación de un atributo UNIQUE con la sintásix de PostgreSQL.
-- Actualización de la definición de una FOREIGN KEY eliminando el seudónimo.
-- Comentando al final de la creación de la tabla el CHARSET.
CREATE TABLE articles (
    id        SERIAL PRIMARY KEY,
    user_id   INT          NOT NULL,
    title     VARCHAR(255) NOT NULL,
    slug      VARCHAR(191) NOT NULL UNIQUE,
    body      TEXT,
    published BOOLEAN DEFAULT FALSE,
    created   TIMESTAMP,
    modified  TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(id)
); -- CHARSET=utf8mb4;

-- Modificando la creación de un atributo UNIQUE con la sintásix de PostgreSQL.
-- Comentando al final de la creación de la tabla el CHARSET.
CREATE TABLE tags (
    id       SERIAL PRIMARY KEY,
    title    VARCHAR(191) UNIQUE,
    created  TIMESTAMP,
    modified TIMESTAMP
); -- CHARSET=utf8mb4;

-- Actualización de la definición de una FOREIGN KEY eliminando el seudónimo.
CREATE TABLE articles_tags (
    article_id INT NOT NULL,
    tag_id     INT NOT NULL,
    PRIMARY KEY (article_id, tag_id),
    FOREIGN KEY (tag_id) REFERENCES tags(id),
    FOREIGN KEY (article_id) REFERENCES articles(id)
);

-- DELETE FROM users;
INSERT INTO users (email, password, created, modified)
VALUES
('cakephp@example.com', 'sekret', NOW(), NOW());

-- En PostgreSQL no se puede agregar el atributo published como 1, por lo cual se elimina.
INSERT INTO articles (user_id, title, slug, body, created, modified)
VALUES
(1, 'First Post', 'first-post', 'This is the first post.', now(), now());